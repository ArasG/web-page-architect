/////////////////..... PAGE SCROLL NAV...../////////////
const btnAbout = document.querySelector('#nav-btn-about')
const btnServices = document.querySelector('#nav-btn-services')
const btnProjects = document.querySelector('#nav-btn-projects')
const btnContact = document.querySelector('#nav-btn-contacts')
const btnTest = document.querySelector('#nav-btn-test')

const sectionIndex = document.querySelector('#section-index')
const sectionAbout = document.querySelector('#section-about')
const sectionServises = document.querySelector('#section-services')
const sectionProjects = document.querySelector('#section-projects')
const sectionContact = document.querySelector('#section-contacts')

btnAbout.addEventListener('click', function (e) {
  e.preventDefault()
  sectionAbout.scrollIntoView({ behavior: 'smooth' })
})

btnServices.addEventListener('click', function (e) {
  e.preventDefault()
  sectionServises.scrollIntoView({ behavior: 'smooth' })
})

btnProjects.addEventListener('click', function (e) {
  e.preventDefault()
  sectionProjects.scrollIntoView({ behavior: 'smooth' })
})

btnContact.addEventListener('click', function (e) {
  e.preventDefault()
  sectionContact.scrollIntoView({ behavior: 'smooth' })
})

/////////////////..... Arrow NAVIGATION ...../////////////
const arrDownInd = document.querySelector('#arr-down-index')
const arrUpAbout = document.querySelector('#arr-up-about')
const arrDownAbout = document.querySelector('#arr-down-about')
const arrUpServices = document.querySelector('#arr-up-services')
const arrDownServices = document.querySelector('#arr-down-services')
const arrUpProjects = document.querySelector('#arr-up-projects')
const arrDownProjects = document.querySelector('#arr-down-projects')
const arrUpContacts = document.querySelector('#arr-up-contacts')

arrDownInd.addEventListener('click', function (e) {
  e.preventDefault()
  sectionAbout.scrollIntoView({ behavior: 'smooth' })
})

arrUpAbout.addEventListener('click', function (e) {
  e.preventDefault()
  sectionIndex.scrollIntoView({ behavior: 'smooth' })
})

arrDownAbout.addEventListener('click', function (e) {
  e.preventDefault()
  sectionServises.scrollIntoView({ behavior: 'smooth' })
})

arrUpServices.addEventListener('click', function (e) {
  e.preventDefault()
  sectionAbout.scrollIntoView({ behavior: 'smooth' })
})

arrDownServices.addEventListener('click', function (e) {
  e.preventDefault()
  sectionProjects.scrollIntoView({ behavior: 'smooth' })
})

arrUpProjects.addEventListener('click', function (e) {
  e.preventDefault()
  sectionServises.scrollIntoView({ behavior: 'smooth' })
})

arrDownProjects.addEventListener('click', function (e) {
  e.preventDefault()
  sectionContact.scrollIntoView({ behavior: 'smooth' })
})

arrUpContacts.addEventListener('click', function (e) {
  e.preventDefault()
  sectionProjects.scrollIntoView({ behavior: 'smooth' })
})

// ///////////////..... Reveal Sections...../////////////
const allSections = document.querySelectorAll('.section-m')
const allNumberFields = document.querySelectorAll('.section-n')

const revealSection = function (entries, observer) {
  const [entry] = entries
  if (!entry.isIntersecting) return
  observer.unobserve(entry.target)
  for (i = 0; i < 4; i++) {
    if (allNumberFields[i].classList.contains('section-hidden')) {
      allNumberFields[i].classList.remove('section-hidden')
      console.log('class removed')
      return
    } else {
      console.log('class not contains')
    }
  }
}

const sectionObserver = new IntersectionObserver(revealSection, {
  root: null,
  threshold: 0.5,
})

allSections.forEach(function (section) {
  sectionObserver.observe(section)
})

// ///////////////..... MOBILE NAV...../////////////
function showHideBobNav() {
  var x = document.getElementById('mMenu')
  if (x.style.display === 'block') {
    x.style.display = 'none'
  } else {
    x.style.display = 'block'
  }
}

const btnAboutM = document.querySelector('#nav-btn-about-mob')
const btnServicesM = document.querySelector('#nav-btn-services-mob')
const btnProjectsM = document.querySelector('#nav-btn-projects-mob')
const btnContactM = document.querySelector('#nav-btn-contacts-mob')

btnAboutM.addEventListener('click', function (e) {
  e.preventDefault()
  sectionAbout.scrollIntoView({ behavior: 'smooth' })
  showHideBobNav()
})

btnServicesM.addEventListener('click', function (e) {
  e.preventDefault()
  sectionServises.scrollIntoView({ behavior: 'smooth' })
  showHideBobNav()
})

btnProjectsM.addEventListener('click', function (e) {
  e.preventDefault()
  sectionProjects.scrollIntoView({ behavior: 'smooth' })
  showHideBobNav()
})

btnContactM.addEventListener('click', function (e) {
  e.preventDefault()
  sectionContact.scrollIntoView({ behavior: 'smooth' })
  showHideBobNav()
})
